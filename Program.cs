using System;

namespace Cubo_Magico
{
  internal class Program
  {
    public static void Main(string[] args)
    {
      int[,] cubo;
      int n1,n2,direccion1=0,direccion2=0,opc1=0,opc2=0,opc3=0,numero=1;
      Console.WriteLine("Cuantas filas son?");
      n1 = Convert.ToInt16(Console.ReadLine());
      Console.WriteLine("Cuantas columnas son?");
      n2 = Convert.ToInt16(Console.ReadLine());
      cubo=new int[n1, n2];
      //llenar la matriz de ceros
      for (int i=0;i<n1;i++)
      {
        for (int j=0;i<n1;i++)
        {
          cubo[i, j] = 0;
        } 
      }
      while (opc1 == 0)
      {
        Console.WriteLine("A donde desa ir? \n1._Izquierda \n2._Derecha");
        opc1 = Convert.ToInt32(Console.ReadLine());
        switch (opc1)
        {
          case 1:
            direccion1 = -1;
            break;
          case 2:
            direccion1 = 1;
            break;
          default:
            Console.WriteLine("Opcion no valida");
            opc1 = 0;
            break;
        }
      }
      while (opc2 == 0)
      {
        Console.WriteLine("A donde desa ir? \n1._Arriba \n2._Abajo");
        opc2 = Convert.ToInt32(Console.ReadLine());
        switch (opc2)
        {
          case 1:
            direccion2 = -1;
            break;
          case 2:
            direccion2 = 1;
            break;
          default:
            Console.WriteLine("Opcion no valida");
            opc2 = 0;
            break;
        }
      }
      //Ahora va lo cabron
      if (n2%2==0)
      {
        while (opc3 == 0)
        {
          Console.WriteLine("El numero de filas es par, desea iniciar con: \n1._Izquierda \n2._Derecha");
          opc3 = Convert.ToInt32(Console.ReadLine());
          switch (opc3)
          {
            case 1:
              cubo[n1, (n2/2)] = numero;
              break;
            case 2:        
              cubo[n1, (n2/2)+1] = numero;
              break;
            default:
              Console.WriteLine("Opcion no valida");
              opc3 = 0;
              break;
          }
        }
      }
      else
      {
        cubo[n1-1, (n2/2)] = numero;
      }
      Console.WriteLine("{0} {1}",direccion1,n2/2);
      Console.WriteLine("{0} {1}",direccion2,n1);
      
    }
  }
}